import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { helloMessage } from '../actions/uploadAction'
import { connect } from 'react-redux'

class UploadComponent extends Component {
    constructor(props) {
        super(props)

    }
    componentDidMount() {
        this.props.helloMessage()
    }
    render() {
        return (
            <React.Fragment>
                {/* <div className="box-model">{this.props.uploadMessage}</div> */}
                {/* <div className="box-model">{this.props.uploadMessage}</div> */}
                {/* <a className="box-model" href="">Siraj</a>
                <a className="box-model" href="">Luqman</a> */}
                <div>
                    <div className="wrapper">
                        <h2>About Me</h2>
                        <p>I am full stack developer</p></div>
                    <div className="wrapper">
                        <h2>About Me</h2>
                        <p>I am full stack developer</p></div>
                    <div className="wrapper">
                        <h2>About Me</h2>
                        <p>I am full stack developer</p></div>
                </div>
            </React.Fragment>
        )
    }

}
export function mapStateToProps(state) {
    const getHelloMessage = state.getIn(['uploadReducer', 'uploadMessage'], '')
    return {
        uploadMessage: getHelloMessage
    }
}

export default connect(mapStateToProps,
    { helloMessage: helloMessage })(UploadComponent)
