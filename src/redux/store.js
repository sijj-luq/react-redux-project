import { createStore, applyMiddleware, compose } from 'redux'
import rootReducer from './rootReducer'
import thunk from 'redux-thunk'
import {Map} from 'immutable'

const middlewares = [thunk]

const store = createStore(rootReducer,Map(),compose(applyMiddleware(...middlewares),
window && window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__() : f => f
))

export default store

